let menu = document.querySelector('.main-navigation');

// Check if the menu element exists
if (menu) {
    let menuOpen = document.querySelector('.menu-open');
    let menuClose = menu.querySelector('.menu-close');

    // Open icon
    menuOpen.addEventListener('click', () => {
        menu.classList.add('open');
        setTimeout(() => {
            menu.classList.add('fade');
        }, 0);
    });

    // Close icon
    menuClose.addEventListener('click', () => {
        menu.classList.remove('fade');
        setTimeout(() => {
            menu.classList.remove('open');
        }, 0);
    });
}
