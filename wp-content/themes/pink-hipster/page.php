<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pinkhipster
 */

get_header();
?>
	<?php while ( have_posts() ) : the_post(); ?>
		<main id="main" class="front-page">
			<section id="page-header">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<h1 class="text-white"><?php echo esc_html( get_the_title() ); ?></h1>
						</div>
					</div>
				</div>
			</section>
			<section>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<?php echo wp_kses_post( get_the_content() ); ?>
						</div>
					</div>
				</div>
			</section>
		</main>
	<?php endwhile; ?>
<?php
get_footer();
