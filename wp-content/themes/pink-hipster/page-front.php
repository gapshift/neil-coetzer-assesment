<?php
/**
 * Template Name: Front Page
 *
 * @category Templates
 * @package  pinkhipster
 * @author   Neil Coetzer <neil@neilcoetzer.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link     https://neilcoetzer.com
 */
get_header();
?>
<main id="main" class="front-page">

<!-- Hero -->
	<?php
	// ACF fields
	$hero_tag         = get_field( 'hero_tag' );
	$hero_title       = get_field( 'hero_title' );
	$hero_description = get_field( 'hero_description' );
	$hero_btn_link    = get_field( 'hero_btn_link' );
	$hero_text_link   = get_field( 'hero_text_link' );
	$hero_image  = get_field( 'hero_image' );
	?>
	<section id="hero">
		<?php if( isset( $hero_image ) && ! empty( $hero_image ) ) : ?>
			<img class="hero-image" src="<?php echo esc_url( $hero_image['url'] ); ?>" alt="<?php echo esc_attr( $hero_image['alt'] ); ?>">
		<?php endif; ?>
		<div class="container text pb-350 pb-md-700 pb-lg-0 pt-60 pt-lg-0">
			<div class="row">
				<div class="col-12 col-lg-6 text-white text-center text-md-left">
					<p class="tag text-green"><?php echo esc_html( $hero_tag ); ?></p>
					<h1 class="text-white"><?php echo esc_html( $hero_title ); ?></h1>
					<p class="text-white"><?php echo wp_kses_post( $hero_description ); ?></p>
					<button class="mb-40 mb-lg-0" onclick="window.open('#','_blank');">Download Now</button>
					<a href="#" target="_blank" class="text-white ml-0 ml-md-20">Some link</a>
				</div>
			</div>
		</div>
	</section>

<!-- Intro -->
	<?php
	// ACF fields
	$intro_tag   = get_field( 'intro_tag' );
	$intro_text  = get_field( 'intro_text' );
	$intro_image = get_field( 'intro_image' );
	?>
	<section id="intro">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-5 d-flex flex-column justify-center">
				<?php if( isset( $intro_image ) && ! empty( $intro_image ) ) : ?>
					<img src="<?php echo esc_url( $intro_image['url'] ); ?>" alt="<?php echo esc_attr( $intro_image['alt'] ); ?>">
				<?php endif; ?>
				</div>
				<div class="col-12 col-md-6 offset-md-1 d-flex flex-column justify-center">
					<?php if( isset( $intro_tag ) ) : ?>
						<p class="tag text-blue"><?php echo esc_html( $intro_tag ); ?></p>
					<?php endif; ?>
					<?php if( isset( $intro_text ) ) : ?>
						<?php echo wp_kses_post( $intro_text ); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>

<!-- Call to action section -->
	<?php
	// ACF fields
	$cta_tag              = get_field( 'cta_tag' );
	$cta_text             = get_field( 'cta_text' );
	$cta_link             = get_field( 'cta_link' );
	$cta_background_image = get_field( 'cta_background_image' );
	?>
	<section id="cta" style="background-image: url('<?php echo esc_url( $cta_background_image['url'] ); ?>');">
		<div class="overlay"></div>
		<div class="container content">
			<div class="row">
				<div class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4 text-center text-white">
					<?php if( isset( $cta_tag ) ) : ?>
						<p class="tag text-green"><?php echo esc_html( $cta_tag ); ?></p>
					<?php endif; ?>
					<?php if( isset( $cta_text ) ) : ?>
						<?php echo wp_kses_post( $cta_text ); ?>
					<?php endif; ?>
					<?php if( isset( $cta_link ) ) : ?>
						<button onclick="window.open('<?php echo esc_url( $cta_link['url'] ); ?>','<?php echo esc_attr( $cta_link['target'] ); ?>');"><?php echo esc_html( $cta_link['title'] ); ?></button>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>

</main>
<?php
get_footer();
