<?php
/**
 * The header for our theme
 *
 * @category Theme
 * @package  pinkhiipster
 * @author   Neil Coetzer <neil@neilcoetzer.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link     https://neilcoetzer.com
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="utf-8"/>
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<link href="http://gmpg.org/xfn/11" rel="profile"/>

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
    <a class="skip-link screen-reader-text" href="#primary">Skip to content</a>

    <header id="masthead" class="site-header">
		<div class="container">
			<div class="row">
				<div class="col-12 d-flex flex-row justify-space-between align-items-center">
					<!-- #logo -->
					<img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/site-logo.svg" alt="Logo">

					<!-- Menu open -->
					<div class="menu-open"></div>

					<!-- #site-navigation -->
					<nav class="main-navigation">
						<!-- Menu close -->
						<div class="menu-close"></div>

						<!-- Menu items -->
						<?php
						wp_nav_menu(
							array(
								'theme_location' => 'main-menu',
								'menu_id'        => 'main-menu',
							)
						);
						?>
					</nav>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->
