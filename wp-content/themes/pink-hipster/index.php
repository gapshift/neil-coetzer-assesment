<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pinkhipster
 */

get_header();
?>
	<?php while ( have_posts() ) : the_post(); ?>
		<main id="main" class="front-page">
			<section id="page-header">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<h1 class="text-white"><?php echo esc_html( get_the_title() ); ?></h1>
						</div>
					</div>
				</div>
			</section>
			<section>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<?php echo wp_kses_post( get_the_content() ); ?>
						</div>
					</div>
				</div>
			</section>
		</main>
	<?php endwhile; ?>
<?php
get_footer();
