<?php
/**
 * Setup the Theme Settings page for ACF
 * -> We'll use this to set the theme logo, etc.
 */
add_action( 'init', 'ph_starter_acf', 1 );
function ph_starter_acf() {
	$theme_section_icon = get_stylesheet_directory_uri() . '/assets/dashicons-theme-settings.svg';

	if ( function_exists( 'acf_add_options_page' ) ) {
		acf_add_options_page(
			array(
				'page_title' => 'Theme Settings',
				'menu_title' => 'Theme Settings',
				'menu_slug'  => 'theme-settings',
				'capability' => 'edit_posts',
				'redirect'   => false,
				'position'   => 3.3,
				'icon_url'   => $theme_section_icon,
			)
		);
	}
}
