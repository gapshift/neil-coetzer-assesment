<?php
/**
 * Enqueue scripts and styles.
 */
function pinkhipster_scripts() {
	$theme   = wp_get_theme();
	$version = $theme->get( 'Version' );

	wp_enqueue_style( 'pinkhipster-style', get_stylesheet_uri(), array(), $version );

	wp_enqueue_script( 'pinkhipster-js', get_template_directory_uri() . '/assets/js/app.min.js', array(), $version, true );
}
add_action( 'wp_enqueue_scripts', 'pinkhipster_scripts' );