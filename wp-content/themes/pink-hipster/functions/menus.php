<?php
/**
 * Register the menus
 * This theme uses wp_nav_menu() in one location i.e. main navigation.
 * */
register_nav_menus(
    array(
        'main-menu' => 'Primary'
    )
);