<?php

/**
 * PinkHipster functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package PinkHipster
 */

if ( ! defined( '_T_DOMAIN' ) ) {
	// Replace the version number of the theme on each release.
	define( '_T_DOMAIN', 'pinkhipster' );
}

if ( ! defined( '_T_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_T_VERSION', '1.0.0' );
}

if ( ! function_exists( 'pinkhipster_setup' ) ) :
    function pinkhipster_setup(){
        /**
         * Use the classic editor instead of Gutemberg
         */
        add_filter( 'use_block_editor_for_post', '__return_false', 10 );

        /*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that the theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
        add_theme_support( 'title-tag' );
    }
endif;
add_action( 'after_setup_theme', 'pinkhipster_setup' );