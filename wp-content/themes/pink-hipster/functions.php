<?php
/**
 * Theme main functions file
 */
get_template_part( 'functions/theme' );
get_template_part( 'functions/acf' );
get_template_part( 'functions/scripts_styles' );
get_template_part( 'functions/menus' );