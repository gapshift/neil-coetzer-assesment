const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const concat = require('gulp-concat');
const babel = require('gulp-babel');
const terser = require('gulp-terser');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const { src, series, parallel, dest, watch } = require('gulp');
const zip = require('gulp-zip');

// Settings object
const settings = {
    paths: {
        src: {
            js: '__dev/js/**/*.js',
            sass: '__dev/sass/**/*.scss',
            theme: [
                './*',
                '!./__dev',
                '!./dist',
                '!stylelintrc.json',
                '!gulpfile.js',
                '!./node_modules',
                '!package.json',
                '!package-lock.json',
                '!style.css.map',
            ],
        },
        dest: {
            js: './assets/js',
            sass: './',
            theme: './dist',
        },
    },
};

// Zip dist theme files
function makeDist() {
    return src(settings.paths.src.theme)
        .pipe(zip('pinkhipster-theme-1.0.0.zip'))
        .pipe(dest(settings.paths.dest.theme));
}

// Watch files
function watchFiles() {
    watch(settings.paths.src.sass, parallel('css'));
    watch(settings.paths.src.js, parallel('js'));
}

// Concatinate, minify, rename and copy
function js() {
    return src(settings.paths.src.js)
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        .pipe(
            babel({
                presets: [
                    [
                        '@babel/env',
                        {
                            modules: false,
                        },
                    ],
                ],
            }),
        )
        .pipe(terser())
        .pipe(rename({ extname: '.min.js' }))
        .pipe(sourcemaps.write('.'))
        .pipe(dest(settings.paths.dest.js));
}

// Concatinate, compile sass, minify and copy
function css() {
    return src(settings.paths.src.sass)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(cleanCSS({ compatibility: 'ie11' }))
        .pipe(sourcemaps.write('.'))
        .pipe(dest(settings.paths.dest.sass));
}
exports.watchFiles = watchFiles;
exports.css = css;
exports.js = js;
exports.makeDist = makeDist;
const build = parallel(js, css);
exports.watch = series(build, watchFiles);
