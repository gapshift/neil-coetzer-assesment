<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package pinkhipster
 */

get_header();
?>
<main id="main" class="page-404">
	<section id="not-found-404" class="d-flex flex-column justify-center align-items-center">
		<div class="container">
			<div class="row">
				<div class="col-12 d-flex flex-column justify-center align-items-center">
					<!-- Header -->
                    <h1 class="text-white text-center"><?php esc_html_e( '404', 'pinkhipster' ); ?></h1>
                    <h3 class="text-white text-regular text-center"><?php echo esc_html('It seems like nothing was found at this location.') ?></h3>
                    <button class="mt-40" onclick="window.open('<?php echo esc_url( site_url() ); ?>','_self');">Back home</button>
				</div>
			</div>
		</div>
	</section>
</main>
<?php
get_footer();
