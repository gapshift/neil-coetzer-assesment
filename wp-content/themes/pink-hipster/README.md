![PinkHipster](./assets/images/readme.png)

# A Custom WordPress Theme

##### v1.0

Created for 10up

## Get started

The theme uses the following development dependencies:

-   NPM
-   Gulp

### Installation

Run `gulp-rename gulp-imagemin gulp-sass gulp-clean-css gulp-concat gulp-terser gulp-sourcemaps gulp-babel @babel/core @babel/preset-env` to install the gulp dependencies.

The following scripts are available:

`npm run start` – Builds/Compiles the JS + SASS files, then starts the watcher

`npm run make-theme` – Builds and compresses the theme files, ready for production/distribution (files are compiled into the 'dist' directory inside the theme).

## Contributing

This is a theme built specifically for 10up and no contribution will be monitored or merged unless requested specifically.

## Contact

neil@neilcoetzer.com
