<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pinkhiipster
 */

?>

<footer class="d-flex flex-row justify-center align-items-center">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/10up_logo.svg" alt="10up Logo">
</footer>

<?php wp_footer(); ?>

</body>

</html>
